//Luke Weaver 1937361
package payroll;

interface Employee {
	double getWeeklyPay();
}
