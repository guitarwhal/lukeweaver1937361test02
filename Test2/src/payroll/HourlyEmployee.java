//Luke Weaver 1937361
package payroll;

public class HourlyEmployee implements Employee{
	private double hourly;
	private int hours;
	
	public HourlyEmployee(double hourly, int hours) {
		this.hourly = hourly;
		this.hours = hours;
	}
	
	//gets the weekly pay
	public double getWeeklyPay() {
		return this.hourly * this.hours;
	}

	//gets the hours worked in the week
	public double getHours() {
		return this.hours;
	}
	
	//gets the hourly wage
	public double getHourly() {
		return this.hourly;
	}
}
