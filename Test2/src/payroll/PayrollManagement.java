//Luke Weaver 1937361
package payroll;

public class PayrollManagement {
	public static void main(String[] args) {
		Employee[] emp = {
				new HourlyEmployee(13.10, 40),
				new UnionizedHourlyEmployee(15, 45, 40, 1.5),
				new SalariedEmployee(39375),
				new HourlyEmployee(18.5, 40),
				new UnionizedHourlyEmployee(17, 39, 40, 1.5)
		};
		
		System.out.println("Total Expenses: " + getTotalExpenses(emp));
	}
	
	//adds up all the salaries and pay of the employees in a week
	public static double getTotalExpenses(Employee[] e) {
		double total = 0;
		
		for(Employee emp : e) {
			total += emp.getWeeklyPay();
		}
		
		return total;
	}
}