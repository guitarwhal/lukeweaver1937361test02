//Luke Weaver 1937361
package payroll;

public class SalariedEmployee implements Employee{
	private double yearly;
	
	public SalariedEmployee(int y) {
		this.yearly = y;
	}
	
	//gets the weekly pay
	public double getWeeklyPay() {
		return this.yearly/52;
	}
	
	//gets the yearly salary
	public double getYearly() {
		return this.yearly;
	}

}
