//Luke Weaver 1937361
package payroll;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private int maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(double hourly, int hours, int max, double overtimeRate) {
		super(hourly, hours);
		this.maxHoursPerWeek = max;
		this.overtimeRate = overtimeRate;
	}

	//gets the weekly pay and adds overtime accordingly
	public double getWeeklyPay() {
		double overtimePay = (this.getHours() - this.maxHoursPerWeek) * (this.getHourly() * this.overtimeRate);
		
		if(this.getHours() <= this.maxHoursPerWeek) {
			return this.getHours() * this.getHourly();
		}
		return (this.getMaxHours() * this.getHourly()) + overtimePay;
	}
	
	//gets the maximum hours before overtime applies
	public int getMaxHours() {
		return this.maxHoursPerWeek;
	}
	
	//gets the overtime rate (e.g. 1.5 = time and a half)
	public double getOvertimeRate() {
		return this.overtimeRate;
	}
}
