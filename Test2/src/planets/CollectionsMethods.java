//Luke Weaver 1937361
package planets;
import java.util.*;

public class CollectionsMethods {
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		ArrayList<Planet> arr = new ArrayList<Planet>();
		
		for(Planet p : planets) {
			if(p.getRadius() >= size) {
				arr.add(p);
			}
		}
		return arr;
	}
}
